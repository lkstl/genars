#lang scribble/manual

@title[#:tag "int"]{Integers}
The @racket[(generic operations int)] library installs the scheme procedure
@racket[exact-integer?] as type-predicate for @racket[Int].
The usual equality @racket[=] is used as equality predicate.
Furthermore, the standard @racket[zero?] predicate, addition @racket[+],
negation @racket[-] and multiplication @racket[*] are installed as integer
operations of their generic counterparts @racket[=zero?], @racket[add],
@racket[neg] resp. @racket[mul].
The @racket[mod] operations uses the scheme procedure @racket[modulo].
The rest of the generic operations are either inherited from their polynomial
counterparts or implemented by the following procedures:

@defproc[(int-div [i1 int?] [i2 int?]) int?]{
  Implements @racket[div].
}
@defproc[(int-pow [i int?] [n (and/c exact-integer? (not/c negative?))]) int?]{
  Implements @racket[pow].
}
@defproc[(int-mod-div [m int?] [i1 int?]
    [i2 int?]) int?]{
  Implements @racket[mod-div]. Uses @racket[make-euclid] to find a solution
  to the equation
  @racket[(= i1 (+ (* i2 s) (* m t)))]
}
@defproc[((int-ldiv [i1 int?] [i2 int?])
   [add procedure?] [neg procedure?] [mul procedure?] [div procedure?])
  int?]{
  Implements @racket[make-ldiv]. Ignores its procedure arguments and simply
  uses the scheme procedure @racket[floor/].
}
  
@defproc[(int-show [i int?]) any]{
  Simply returns @racket[i].
}
