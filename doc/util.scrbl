#lang scribble/manual

@title[#:tag "util"]{Utilities}
The library @racket[(util table)] implements persistent tables modeled
as nested association lists.
The library @racket[(util lambda)] provides some higher order procedures
like composition of procedures and an identity procedure.

@section{Association lists}
@defproc[(alist [key any/c] [value any/c] ...) alist?]{
        Analogue of @racket[list] for association lists.
        Uses @racket[alist-cons] from SRFI 1.
        Expects an even number of arguments, alternating between keys
          and values.
        
        Example:
        @racketblock[> (alist 0 'a 1 'b 0 'c)
                     ((0 . 'a) (1 . 'b) (0 . 'c))]
        }
@defproc[(alist? [x any/c]) boolean?]{
        Returns @racket[#t] if @racket[x] is an association list,
        that is, a list of pairs.
        }

@section{Tables}
Whereas an association list associates a value to a single key, a value
stored in a table is associated to multiple keys. More precisely, it is
associated to an ordered list of keys. One can treat association lists
as tables by identifying a key with list of length 1 containing it.
Similarly, one can identify any value with the table whose only entry
is this value associated to the empty list of keys (which is exactly
what happens in our implementation, except for values which are alists).
Consequently, we implement tables as nested association lists.

@defproc[(table [keys list?] [value any/c] ...) table?]{
  Iteratively inserts given values under the given keys using
  @racket[table-insert] to create a newly allocated table.
  Expects an even number of arguments, alternating between lists of keys
  and values.
  The arguments are processed from left to right, that is,
  the leftmost value is inserted first.
  
  Example:
  @racketblock[(table (list 0 0) 'aa (list 0 1) 'ab (list 0 0) 'hello)]
  evaluates to
  @racketblock[((0 (0 . 'hello) (1 . 'ab) (0 . 'aa)) (0 (1 . 'ab) (0 . 'aa)) (0 (0 . 'aa)))]
}
@defproc[(table? [x any/c]) boolean?]{
        Returns @racket[#t] if @racket[x] is a table, that is, 
        a list of pairs whose car is a list.
        }
@defproc[(table-lookup [keys list?] [tbl table?]) any/c]{
        Returns the first entry in table whose list of keys coincides
        with the given @racket[keys].
        If no such entry is found, returns @racket[#f].
        }
@defproc[(table-insert [keys list?] [value any/c] [tbl table?]) table?]{
        Returns a newly allocated table which has the same entries as
        @racket[tbl] plus the entry with value @racket[value] stored under
        @racket[keys].
        }

@section{Lambda}
@defproc[(lambda-constant [x any/c]) any]{
  Returns a procedure which takes any number of arguments and returns 
  @racket[x].
  }
@defproc[(lambda-identity [arg any/c] ...) any]{
  Returns the given arguments as values. Thus, it is an alias for the
  @racket[values] procedure.
  }
@defproc[(lambda-compose [f procedure?] ...) any]{
  Returns the composition of the given arguments.
  
  Example: 
  @racketblock[> ((lambda-compose - + floor/) 11 3)
               -5]
  }
