#lang scribble/manual

@title[#:tag "operations"]{Generic operations}
  The library @racket[(generic operations)] implements a generic arithmetic
  system for multivariate polynomials over the integers.
  It is based on the generic arithmetic system described in chapter 2.5
  of @cite{SICP}. 

  The library defines a list @racket[type-tower] of symbols which contains
  the names of the types of our system.
  There are only two types @racket[Int] and @racket[Poly].
  We call these types @italic{arithmetic types}.
  The @racket[type-tower] defines an ordering on the set of arithmetic-types,
  where @racket[Int] is lower than @racket[Poly].

  Furthermore, the library defines a table @racket[operation-table],
  which stores various procedures, implementing the generic operations
  described further below.
  The table is two-dimensional with the first key being the name of an
  operation, and the second one a list of its arguments' types.
  It is initially empty.

@section{Installing & looking up operations}
@defproc[(install-operation! [proc procedure?] [name symbol?]
            [type symbol?] ...) any]{
  Installs the procedure @racket[proc] as an operation named @racket[name]
  for the given @racket[type]s in the @racket[operation-table].
  }
@defproc[(lookup-operation [name symbol?] [type symbol?] ...)
         (or/c procedure? #f)]{
  Returns operations named @racket[name] for the given @racket[type]s 
  from the operation-table. 
  If no such operation is found, returns @racket[#f].
  }

@section{Type predicates & Equality}
@defproc[(int? [x any/c]) boolean?]{
  Returns @racket[#t] if @racket[x] is of type @racket[Int].
  }
@defproc[(poly? [x any/c]) boolean?]{
  Returns @racket[#t] if @racket[x] is of type @racket[Poly].
  }
@defproc[(arithmetic-value? [x any/c]) boolean?]{
  Returns @racket[#t] if @racket[x] belongs to one of the arithmetic-types,
  i.e. if @racket[x] is of type @racket[Int] or @racket[Poly].
  }
@defproc[(detect-type [x any/c]) (or/c symbol? #f)]{
  Returns the type of @racket[x] if possible, otherwise @racket[#f].
  }
@defproc[(=? [x arithmetic-value?] [y arithmetic-value?]) boolean?]{
  Tests equality of @racket[x] and @racket[y].
  }

@section{Lifting, Projection & Simplification}
@defproc[(lift [x arithmetic-value?]) arithmetic-value?]{
  Lifts @racket[arg] to the next higher type if possible, otherwise
  signals an error.
  }
@defproc[(project [x arithmetic-value?]) arithmetic-value?]{
  Projects @racket[x] to a "simpler" value.
  This does not necessarily change the type of @racket[x]
  (contrary to the description of @racket[project] in @cite{SICP}
   Exercise 2.85).
  }
@defproc[(simplify [x arithmetic-value?]) arithmetic-value?]{
  Simplifies @racket[x] as much as possible.
  More precisely, @racket[x] is @racket[project]ed and the result @racket[x*]
  is compared to @racket[x] using @racket[=?] (in the process of this, 
  @racket[x*] may be lifted again).
  If the comparison returns @racket[#t], this means, that projecting
  @racket[x] has not changed it with respect to @racket[=?] and no necessary
  information was lost.
  We can thus repeat this process of projecting and comparing until
  the input of the current iteration and its projection differ, at which point
  the input is returned.
  
  The advantage of our approach in contrast to the approach of Exercise 2.85 in
  @cite{SICP} it is possible to simplify values without changing their type.
  For example, the following polynomial in the variable @racket[y]
  @codeblock{(x²+x+1)y⁰}
  is constant and may just as well be written as
  @codeblock{x²+x+1}
  which is certainly simpler but still a polynomial and thus of the same type.
}

@section{Generic application}
@defproc[(apply-generic [op symbol?] [arg symbol?] ...) any]{
  Applies the generic operation of name @racket[op] to the given arguments
  @racket[arg]. To this end, @racket[apply-generic] first detects the
  @racket[types] of the arguments using @racket[detect-type] and looks up
  the operation @racket[op] for these types in the @racket[operation-table].
  If no operation is found, the common highest type of the
  @racket[types] is computed and every argument is @racket[lift]ed to this
  type. If @racket[op] does not exist for these types either, all arguments
  are lifted to the next higher type in the type tower until @racket[op]
  exists for the argument types or there is no higher type.
  In the first case the procedure corresponding to @racket[op] is applied
  to the (@racket[lift]ed) @racket[arg]uments.
  In the second case an error is signaled.

  Lifting arguments to their maximal type allows us to apply generic
  operations to arguments of different types, without having to implement
  the operation for every possible type combination. (At least, this is
  a partial solution. Of course, lifting every argument to their common
  max-type may be too much in some applications.)
  Repeatedly lifting to higher types implements some sort of inheritance
  for generic operations, sparing us from having to implement a generic
  operation for a lower type if it is already implemented for a higher type.

  If possible the result of the application is @racket[simplifi]ed.
  
  This procedure is based on the approach to generic application presented in
  Exercises 2.83-2.85 of @cite{SICP}.
  }

@section{Arithmetic}
@defproc[(=zero? [x arithmetic-value?]) boolean?]{
  Returns @racket[#t] if and only if @racket[x] is zero.
  }
@deftogether[(
@defproc[(add [x arithmetic-value?] ...) arithmetic-value?]
@defproc[(neg [x arithmetic-value?]) arithmetic-value?]
@defproc[(mul [x arithmetic-value?] ...) arithmetic-value?]
@defproc[(div [x arithmetic-value?] [y arithmetic-value?]) arithmetic-value?]
@defproc[(pow [x arithmetic-value?]
 [n (and/c exact-integer? (not/c negative?))]) arithmetic-value?])]{
  Basic arithmetic operations: addition, negation, multiplication, division
  and exponentiation.
  }

@section{Modular arithmetic}
@defproc[((mod [m arithmetic-value?]) [x arithmetic-value?])
          arithmetic-value?]{
  Reduces @racket[x] modulo @racket[m], that is, returns a representative
  of the residue class of @racket[x] with respect to @racket[m].
  The modulus @racket[m] is expected to be an integer or a power
  of a linear factor, see @racket[poly-mod] in @secref{poly} for more
  details.
  For normalization, @racket[(mod m)] chooses @racket[0] as a representative
  for the zero residue class.
  }
@defproc[((mod=zero? [m arithmetic-value?]) [x arithmetic-value?]) boolean?]{
  Returns @racket[#t] if and only if @racket[x] is zero modulo @racket[m].
  Since @racket[(mod m)] evaluates to @racket[0] on the zero residue class,
  @racket[(mod=zero? m)] simpliy reduces its argument and applies
  @racket[=zero?] afterwards.
  }
@deftogether[(
@defproc[((mod-add [m arithmetic-value?])
  [x arithmetic-value?] ...) arithmetic-value?]
@defproc[((mod-neg [m arithmetic-value?])
  [x arithmetic-value?]) arithmetic-value?]
@defproc[((mod-mul [m arithmetic-value?])
  [x arithmetic-value?] ...) arithmetic-value?]
@defproc[((mod-pow [m arithmetic-value?])
  [x arithmetic-value?] [n exact-integer?]) arithmetic-value?])]{
  Modular analogues of the ordinary basic arithmetic operations above.
  All operations @racket[(mod-op m)] reduce their arguments modulo @racket[m],
  apply the corresponding ordinary operation @racket[op] and reduce the 
  result.
  }
@defproc[((mod-div [m arithmetic-value?])
  [x arithmetic-value?] [y arithmetic-value?]) arithmetic-value?]{
  Returns @racket[q] such that
  @racketblock[(=zero? ((mod m) (add (mul q y) (neg x))))]
  evaluates to @racket[#t].
  Since divisions in a quotient ring may be possible which are not possible
  in the base ring, an implementation @racket[mod-div] must rely on a special
  procedure, unlike the other modular variants of ordinary arithmetic
  operations.
  }

@section{Long division}
@defproc[((make-ldiv [add procedure?] [neg procedure?] [mul procedure?]
    [div procedure?]) [x arithmetic-value?] [y arithmetic-value?])
  (values arithmetic-value? arithmetic-value?)]{
  If possible, returns a quotient @racket[q] and a remainder @racket[r]
  for division of @racket[x] by @racket[y], with respect to the given
  arithmetic operations @racket[add], @racket[neg], @racket[mul], @racket[div].
  This means, the expression
  @racketblock[(=zero? (add (mul q y) r (neg x)))]
  evaluates to @racket[#t].
  
  Furthermore, if @racket[x] and @racket[y] are integers (polynomials),
  so are @racket[q] and @racket[r] and the absolute value (degree) of
  @racket[r] is smaller than that of @racket[y].

  In case @racket[x] and @racket[y] are polynomials in different variables,
  one of them is assumed to be a constant polynomial in the lexicographically
  larger variable.
  Similarly, if one of the arguments is a polynomial and the other is an
  integer, the integer is assumed to be a constant polynomial.
  }

@section[#:tag "euclid"]{Extended Euclidean algorithm}
@defproc[((make-euclid [add procedure?] [neg procedure?] [mul procedure?]
                  [div procedure?])
          [x arithmetic-value?] [y arithmetic-value?])
         (values arithmetic-value? arithmetic-value? arithmetic-value?)]{
  Returns a greatest common divisor @racket[d] of @racket[x] and @racket[y]
  with respect to the given multiplication @racket[mul].
  Additionally, returns @racket[s] and @racket[t] which satisfy Bézout's
  identity with respect to @racket[d], that is,
  @racketblock[(=zero? (add (mul x s) (mul y t) (neg d)))]
  evaluates to @racket[#t].  
  The arguments @racket[x] and @racket[y] should be integers or univariate
  polynomials over a finite field.

  Example:
  We define two polys (see @secref{poly})
  @racketblock[
    (define g (poly 'x 0 (termlist (term 2 1) (term 1 1) (term 0 1))))
    (define h (poly 'x 0 (termlist (term 3 1) (term 2 -1) (term 0 -1))))
  ]
  Then evaluating
  @racketblock[
    ((make-euclid (mod-add 11) (mod-neg 11) (mod-mul 11) (mod-div 11)) g h)
  ]
  yields three arithmetic-values, the first one being @racket[1], so 
  @racket[g] and @racket[h] are coprime modulo @racket[11], and the
  second and third one being equal to the respective value of
  @racketblock[
    (define s (poly 'x 0 (termlist (term 2 1) (term 1 9) (term 0 1))))
    (define t (poly 'x 0 (termlist (term 1 10))))
  ]
  }

@section{Solving linear equations in two variables}
@defproc[((make-solve [add procedure?] [neg procedure?] [mul procedure?]
                          [div procedure?])
              [g arithmetic-value?] [h arithmetic-value?]
              [s arithmetic-value?] [t arithmetic-value?]
              [a arithmetic-value?]) arithmetic-value?]{
  Returns @racket[s*], @racket[t*] such that
  @racketblock[(=zero? (add (mul g s*) (mul h t*) (neg u)))]
  evaluates to @racket[#t].
  Moreover, the absolute value (degree) of @racket[t*]
  is smaller than that of @racket[g].
  
  The arguments @racket[g], @racket[h], @racket[s], @racket[t] are expected
  to satisfy Bézout's identity with respect to @racket[1]:
  @racketblock[(=zero? (add (mul g s) (mul h t) (neg 1)))]
  }

@section{Hensel lifting}
@defproc[(hensel
          [m arithmetic-value?] [j (and/c exact-integer? positive?)]
          [f arithmetic-value?]
          [g arithmetic-value?] [h arithmetic-value?]
          [s arithmetic-value?] [t arithmetic-value?])
         (values arithmetic-value? arithmetic-value? arithmetic-value?
                 arithmetic-value?)]{
  Given @racket[f], @racket[g], @racket[h] and a modulus @racket[m]
  such that
  @racketblock[((mod=zero? m) (add (mul g h) (neg f)))]
  and @racket[s], @racket[t] such that
  @racketblock[((mod=zero? m) (add (mul g s) (mul h t) (neg 1)))]
  as well as a positive integer @racket[j]
  the procedure returns @racket[g*], @racket[h*], @racket[s*], @racket[t*]
  such that
  @racketblock[((mod=zero? (pow m j)) (add (mul g* h*) (neg f)))]
  and
  @racketblock[((mod=zero? (pow m j)) (add (mul g* s*) (mul h* t*) (neg 1)))]
  
  The algorithm is an implementation of the Quadratic Hensel algorithm described
  in section 8.c. of @cite{MPF}.

  Example:
  If we define
  @racketblock[(define f (poly 'x 0 (termlist (term 5 1) (term 2 9) (term 1 10) (term 0 10))))]
  the product @racket[(mul g h)]  will be equal to @racket[f] modulo
  @racket[11], where @racket[g] and @racket[h] are defined as
  in the section on the @secref{euclid}.
  Moreover, the polys @racket[s] and @racket[t] defined there, satisfy
  the second requirement to Hensel lifting.
  Evaluating
  @racketblock[(hensel 11 3 f g h s t)]
  yields polys @racket[g*], @racket[h*], @racket[s*], @racket[t*] making
  both of the following expressions evaluate to @racket[#t]:
  @racketblock[((mod=zero? (pow 11 3)) (add (neg f) (mul g* h*)))
               ((mod=zero? (pow 11 3)) (add -1 (mul g* s*) (mul h* t*)))]
  }

@section{Taylor expansion}
@defproc[((taylor [v symbol?] [c exact-integer?]) [p arithmetic-value?])
   arithmetic-value?]{
  Returns the taylor expansion of @racket[p] centered at @racket[c]
  in the variable @racket[v].
  }

@section{Miscellaneous}
@defproc[(show [x arithmetic-value?]) any]{
  Converts @racket[x] to a self-printing format.
  }
