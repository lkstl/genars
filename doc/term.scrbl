#lang scribble/manual

@title[#:tag "Terms"]{Terms}
The @racket[(generic operations term)] library provides procedures to construct
and operate on @italic{terms} and @italic{termlists}.
A term is a pair consisting of a nonnegative integer, its @italic{order},
and an arithmetic-value, its @italic{coefficient}.
We say that a term @racket[t] is nonzero, if its coefficient is, that is, if
  @racketblock[(=zero? (get-coeff t))]
evaluates to @racket[#f].
A termlist is a list of nonzero terms, which is assumed to be strictly
  descending by term-order.

Algebraically, a univariate polynomial is fully determined by its list of
coefficients, including zero coefficients.
Equivalently, we may only remember the nonzero coefficients and their order,
so any polynomial is uniquely determined by a termlist
 (its @italic{sparse} representation).
By recursion, multivariate polynomials can then be represented as termlist whose
terms have polynomials as coefficients.
In the following, we describe several procedures which compute analogues of
abstract polynomial operations for termlists.

In the next section we use these procedures to implement the same operations
for our actual representation of polynomials, which store a bit more
information, namely, the variable and the expansion point of a polynomial.
For now, however, we do not need this extra information and represent
polynomials by termlists.

Example:
The termlist
@racketblock[((3 . 1) (1 . -1) (0 . 2))]
represents the polynomial @code{f=x³-x+2}.

@section{Construction}
@defproc[(term [order (and/c exact-integer? (not/c negative?))]
               [coeff arithmetic-value?]) term?]{
  Constructs a term. An alias for @racket[cons].
}
@defproc[(termlist-cons [t term?] [ts termlist?]) termlist?]{
  Returns the termlist which has @racket[t] as its @racket[car] and @racket[ts]
  as its @racket[cdr], given that @racket[t] is nonzero, otherwise returns
  @racket[ts].
  
  In case @racket[ts] is not empty, the order of @racket[t] is expected to
  be larger than that of the @racket[car] of @racket[ts].
  This is not checked by @racket[termlist-cons], so it is possible to construct
  illegal termlists, that is, termlists not ordered descendingly by term-order
  (this will be useful to define division for termlists but should not be used
  elsewhere).
}
@defthing[termlist-null termlist?]{
  The empty termlist.
  }
@defproc[(termlist [t term?] ...) termlist?]{
  An analogue of the scheme procedure @racket[list] for termlists,
  using @racket[termlist-cons] and @racket[termlist-null].
  }

@section{Predicates}
@defproc[(term? [x any/c]) boolean?]{
  Tests whether @racket[x] is a term.
  }
@defproc[(termlist? [x any/c]) boolean?]{
  Tests whether @racket[x] is a termlist.
  }

@section{Selection}
@defproc[(get-order [t term?]) (and/c exact-integer? (not/c negative?))]{
  An alias for @racket[car].
  }
@defproc[(get-coeff [t term?]) arithmetic-value?]{
  An alias for @racket[cdr].
  }

@section{Mapping}
@defproc[(termlist-map [order-op procedure?] [coeff-op procedure?]
                       [ts termlist?]) termlist?]{
  An analogue of the scheme procedure @racket[map] for termlists.
  Both @racket[order-op] and @racket[coeff-op] expect a term as their single
  argument and should return a nonnegative integer (the order of the new term),
  respectively an arithmetic value (the coefficient of the new term).
  }

@section{Arithmetic}
@defproc[((termlist-add [add procedure?]) [ts1 termlist?] [ts2 termlist?])
  termlist?]{
  Returns a termlist which represents the addition of the two polynomials
  represented by @racket[ts1] and @racket[ts2].

  The argument @racket[add] is used for coefficiont addition.
  For example, we could choose the generic operation @racket[add] for this task
  or a modular addition procedure like @racket[(mod-add 5)].
  }
@defproc[((termlist-neg [neg procedure?]) [ts termlist?]) termlist?]{
  Returns a termlist which represents the negation
  (with respect to the coefficient negation @racket[neg]) of the polynomial
  represented by @racket[ts].
  }
@defproc[((termlist-mul [add procedure?] [mul procedure?])
   [ts1 termlist?] [ts2 termlist?]) termlist?]{
  Returns a termlist which represents the multiplication of the polynomials
  represented by @racket[ts1] and @racket[ts2].
  By the usual formula for polynomial multiplication, @racket[termlist-mul]
  depends on a coefficient addition and a coefficient multiplication procedure.
  }
@defproc[((termlist-div [add procedure?] [neg procedure?] [mul procedure?]
  [div procedure?]) [ts1 termlist?] [ts2 termlist?]) termlist?]{
  Calls long division @racket[termlist-ldiv] with the given arguments
  and returns the quotient if the remainder is zero.
  }

@section{Modular arithmetic}
@defproc[(termlist-mod [ts termlist?] [tsm termlist?]) termlist?]{
  Returns a termlist which represents the reduction of the polynomial 
  represented by @racket[ts] modulo the polynomial represented by @racket[tsm].

  The modulus @racket[tsm] is expected to contain at most one term @racket[t],
  which is @italic{constant} or @italic{monic}, meaning that either its
  order vanishes or its coefficient is equal @racket[=?] to 1.
  Depending on whether @racket[t] is constant or monic,
  @racket[ts] is either termwise reduced modulo @racket[t]'s coefficient
  or truncated above (including) @racket[t]'s order.
  
  Thus, @racket[termlist-mod] models reduction modulo a constant or a power
  of a linear factor.
  }

@defproc[(termlist-mod-div [tsm termlist?] [ts1 termlist?] [ts2 termlist?])
  termlist?]{
  Computes the division of the polynomial represented by @racket[ts1] 
  and @racket[ts2] when viewed as elements of a polynomial ring quotiened
  by the polynomial represented by @racket[tsm].

  Depending on whether @racket[tsm] represents a constant @racket[m]
  or @racket[n]-th power of a linear factor,
  @racket[termlist-mod-div] calls long division
  @racket[termlist-ldiv] with the generic operations
  @racket[(add-mod m)], @racket[(neg-mod m)], @racket[(mul-mod m)]
  and @racket[(div-mod m)], or calls the helper procedure
  @racket[termlist-div<] described next.
  }

@defproc[((termlist-div< [n (and/c exact-integer? (not/c negative?))]
  [add procedure?] [neg procedure?] [mul procedure?] [div procedure?])
  [ts1 termlist?] [ts2 termlist?]) termlist?]{
  Returns the termlist which represents the division of @racket[ts1] by
  @racket[ts2], if we view those as representing @italic{truncated} polynomials
  with maximal degree @racket[n-1].

  Truncated division works essentially like long division, except that the
  termlists are reversed at the start (so the term-order is ascending instead
  of descending) and the termination condition is different.
  }
  
  
@section{Long division}
@defproc[((termlist-ldiv [add procedure?] [neg procedure?] [mul procedure?]
  [div procedure?]) [ts1 termlist?] [ts2 termlist?])
  (values termlist? termlist?)]{
  Returns two termlists @racket[tsq], @racket[tsr] such that
    @racketblock[((termlist-add add)
                  ((termlist-neg neg) ts1)
                  ((termlist-add add) ((termlist-mul add mul) tsq ts2) tsr))]
  is the empty termlist.
  Thus, @racket[termlist-ldiv] computes termlists which represent the quotient
  and remainder of long division of the polynomial represented by @racket[ts1]
  by the polynomial represented by @racket[ts2].
  }

@section{Taylor expansion}
@defproc[(termlist-ev [c int?] [ts termlist?]) arithmetic-value?]{
  Evaluates the polynomial with expansion point @racket[0] represented by
  @racket[ts] at the point @racket[c].
  
  Example:
  @racketblock[(termlist-ev 2 (termlist (term 2 1) (term 1 -1) (term 0 1)))]
  evaluates to @racket[3]. 
  }

@defproc[(termlist-hasse [k (and/c exact-integer? (not/c negative?))]
  [ts termlist?]) termlist?]{
  Returns the @racket[k]-th Hasse derivative of the polynomial represented
  by @racket[ts].
  }

@defproc[(termlist-taylor [c int?] [ts termlist?]) termlist?]{
  Computes the terms of the taylor expansion at @racket[c]
  of the polynomial with expansion point @racket[0] represented by @racket[ts].

  Uses the fact that the @racket[k]-th coefficient of the taylor expansion
  of a polynomial over any commutative ring is given by the evaluation
  of its @racket[k]-th Hasse derivative at the expansion center.

  Example:
  The taylor expansion of the polynomial @code{x³-x+6} at @racket[-2]
  is @code{(x+2)³-6(x+2)²+11(x+2)}.
  Thus it makes sense that
  @racketblock[(termlist-taylor -2 (termlist (term 3 1) (term 1 -1) (term 0 6)))]
  evaluates to
  @racketblock[((3 . 1) (2 . -6) (1 . 11))]
  }
