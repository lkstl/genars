#lang scribble/manual

@title[#:tag "poly"]{Polynomials}
We wish to model the ring of multivariate polynomials over the integers
whose set of variables is the set of all @racket[scheme] symbols 
(together with the boolean value @racket[#f] we need for technical
purposes).
To this end, the library @racket[(generic operations poly)] defines the
@racket[Poly] record type and with it the following procedures
(internal constructor, predicate and selectors):
@deftogether[(
  @defproc[(poly [var (or/c symbol? #f)] [center int?]
      [terms termlist?]) poly?]
  @defproc[(poly? [x any/c]) boolean?]
  @defproc[(get-var [p poly?]) (or/c symbol? #f)]
  @defproc[(get-center [p poly?]) int?]
  @defproc[(get-terms [p poly?]) termlist?])]
Thus, a record of @racket[Poly] has three fields, its @italic{variable},
its @italic{center} and its @italic{termlist}.

Example: The polynomial @code{x³-x+6} is represented by the record
@racketblock[(poly 'x 0 (termlist (term 3 1) (term 1 -1) (term 0 6)))]
Its taylor expansion around the point @racket[-2] is given by
@code{(x+2)³-6(x+2)²+11(x+2)} which is represented by
@racketblock[(poly 'x -2 (termlist (term 3 1) (term 2 -6) (term 1 11)))]
Furthermore, the polynomial @code{y³-y+6} is represented by
@racketblock[(poly 'y 0 (termlist (term 3 1) (term 1 -1) (term 0 6)))]

We see that our record type models not the polynomial ring described above,
but more precisely the set of taylor expansions of such polynomials
(with exact-integers as centers).
This extra information is beneficial for modular arithmetic.

Since coefficients of terms in a termlist may be arbitrary arithmetic values,
in particular, they may be @racket[poly]s.
So a @racket[poly] indeed serves to model @italic{multivariate} polynomials
by recursion.

Note that the set of all variables can be ordered lexicographically
with initial element @racket[#f].
We thus impose the following restriction:
The variable of a poly @racket[p] must be larger than the variable of any poly
occuring as coefficient in @racket[p]'s termlist.
For example, both expressions
@racketblock[(poly 'y 0 (termlist (term 1 (poly 'x 0 (termlist (term 1 1))))))
             (poly 'x 0 (termlist (term 1 (poly 'y 0 (termlist (term 1 1))))))]
evaluate to a poly which represents the polynomial @racket[xy] but only the
first one is legal.
Furthermore, a poly in the initial variable @racket[#f] should always
be constant, see @racket[poly-embed] further below.

@section{Equality}
@defproc[(poly=? [p1 poly?] [p2 poly?]) boolean?]{
  Tests if the records @racket[p1] and @racket[p2] represent equal 
  polynomials, meaning the variables of the polynomials are equal and
  they have the same coefficients.

  This is less strict than simply calling @racket[equal?] on the records
  representing the polynomials, as records may have different centers
  but represent the same polynomial (i.e. they represent different
  taylor expansions of polynomials).
  }

@section{Lifting & Projecting}
@defproc[((poly-embed [v* (or/c symbol? #f)]) [p arithmetic-value?]) poly?]{
  If @racket[p] is not a poly, returns the constant poly with value @racket[p]
  (i.e. with termlist @racket[(termlist (term 0 p))])
  in the variable @racket[v*] centered at @racket[0].
  If @racket[p] is a poly, @racket[poly-embed] returns the constant poly
  with value @racket[p] if its variable @racket[v] is smaller than @racket[v*],
  returns @racket[p] if their variables are equal and signals an error
  if @racket[v] and @racket[v*] are equal.

  We install @racket[(poly-embed #f)] as a @racket[lift] operation for
  the arithmetic type @racket[Int].
  Thus, any int can be viewed as a constant poly with variable @racket[#f].
}

@defproc[(const-coeff [p poly?]) arithmetic-value?]{
  Returns the constant coefficient of the taylor expansion of the polynomial
  represented by @racket[p].
  
  Example:
  @racketblock[(const-coeff (poly 'x 0 (termlist (term 1 1) (term 0 -1))))]
  evaluates to @racket[-1].
  @racketblock[(const-coeff (poly 'x 1 (termlist (term 1 1))))]
  evaluates to @racket[0].
  (Both polys represented the same polynomial!)
  
  Example:
  @racketblock[(let ((x (poly 'x 0 (termlist (term 1 1))))
                     (xy+x (poly 'y 0 (termlist (term 1 x) (term 0 x)))))
                (const-coeff xy+x))]
  evaluates to the same value as @racket[x] does.
  That means, @racket[const-coeff] returns the constant coefficient in the
  main variable of its argument, not the total constant coefficient.

  The @racket[const-coeff] procedure is installed as a
  @racket[project] operation for @racket[Poly].
  The last example shows, that it may not change the arithmetic type of its
  argument (i.e. lowering it to @racket[Int]), however it will reduce the
  variable order, thus "simplifying" its argument.
  }


@section[#:tag "Variable ordering"]{Variable ordering}
@deftogether[(
    @defproc[(var<? [v1 (or/c symbol? #f)] [v2 (or/c symbol? #f)]) boolean?]
    @defproc[(var>? [v1 (or/c symbol? #f)] [v2 (or/c symbol? #f)]) boolean?])]{
  Tests if @racket[v1] is smaller/larger than @racket[v2].
  The totally ordered set of variables is the set of scheme symbols,
  ordered by
  lexicographical ordering, together with an initial element,
  represented by the boolean value @racket[#f].
  }

@section{Applying termlist-operations}
@defproc[(poly-apply [termlist-op procedure?] [p poly?] ...) any]{
  Returns a poly (or multiple polys) which result from applying the operation
  @racket[termlist-op] to the termlists of @racket[p].
  More precisely, we find the polynomial @racket[p*] among the @racket[p]s
  with the largest variable @racket[v*]
  (if there are multiple with the same variable, pick the leftmost one in 
  the list of arguments),
  convert every other poly to that variable using
  @racket[(poly-embed v*)], taylor expand every poly around the center
  @racket[c*] of @racket[p*], using @racket[poly-taylor],
  and finally apply @racket[termlist-op] to the termlists
  of these polys.
  The resulting termlist(s) are then used to define the output polys
  in the variable @racket[v*] centered at @racket[c*].
  }

@section{Arithmetic}
@defproc[(poly=zero? [p poly?]) boolean?]{
  Tests whether @racket[p] is zero.
  This is by definition the case if and only if @racket[p]'s termlist
  is empty.
  }
@defproc[(poly-add [p1 poly?] [p2 poly?]) poly?]{
  Returns a poly which represents the addition of the polynomials represented
  by @racket[p1] and @racket[p2].
  Uses @racket[poly-apply] and @racket[(termlist-add add)].
  }
@defproc[(poly-neg [p poly?]) poly?]{
  Returns a poly which represent the negation of the polynomial represented
  by @racket[p].
  }
@defproc[(poly-mul [p1 poly?] [p2 poly?]) poly?]{
  Returns a poly which represents the multiplication of the polynomials
  represented by @racket[p1] and @racket[p2].
  }
@defproc[(poly-div [p1 poly?] [p2 poly?]) poly?]{
  Returns a poly which represents the result of dividing the polynomial
  represented by @racket[p1] by the polynomial represented by @racket[p2],
  if possible.
  }
@defproc[(poly-pow [p poly?] [n (and/c exact-integer? (not/c negative?))])
   poly?]{
  Returns a poly which represents the @racket[n]-th power of the polynomial
  represented by @racket[p].
  }

@section{Modular arithmetic}
@defproc[(poly-mod [p poly?] [m poly?]) poly?]{
  Returns a poly which represents the result of reducing the polynomial
  represented by @racket[p] modulo the polynomial represented by @racket[m].
  
  The modulus @racket[m] is expected to be constant or the taylor expansion
  of a power of a linear factor at its root.

  Example:
  @racketblock[(let ((f (poly 'x -2 (termlist (term 3 1) (term 2 -6) (term 1 11))))
                     (x+2 (poly 'x -2 (termlist (term 1 1)))))
                  (poly-mod f (poly-pow x+2 2)))]
  evaluates to the same value as
  @racketblock[(poly 'x -2 (termlist (term 1 11)))]
  However,
  @racketblock[(let ((f ...)
                     (x²+4x+4 (poly 'x 0 (termlist (term 2 1) (term 1 4) (term 0 4)))))
                  (poly-mod f x²+4x+4))]
  signals an error although @racket[(poly-pow x+2 2)] and @racket[x²+4x+4]
  represent the same power of a linear factor because the first one has
  the correct center (its root @racket[-2]) while the second has not.
  }
@defproc[(poly-mod-div [m poly?] [p1 poly?] [p2 poly?]) poly?]{
  Returns a poly which represents the result of dividing the polynomial
  represented by @racket[p1] by the polynomial represented by @racket[p2],
  where both polynomials are regarded as representing residue classes 
  modulo @racket[m] (that is, @racket[p1] and @racket[p2] represent elements
  of the quotient ring of multivariate polynomials quotiened by the
  polynomial represented by @racket[m]).
  }

@section{Long Division}
@defproc[((poly-ldiv [p1 poly?] [p2 poly?])
  [add procedure?] [neg procedure?] [mul procedure?] [div procedure?])
  (values poly? poly?)]{
  Returns two polys @racket[q] and @racket[r] which are the result
  of long dividing the polynomial represented by @racket[p1] by the
  polynomial represented by @racket[p2], if possible.

  The underlying coefficient ring structure is supplied by the
  procedures @racket[add], @racket[neg], @racket[mul], @racket[div],
  which allows us to divide polys whose coefficients may represented
  not ordinary polynomials in lower variables or integers, but 
  truncated polynomials (in lower variables) or modular integers.
  }

@section{Taylor expansion}
@defproc[((poly-taylor [p poly?]) [v* (or/c symbol? #f)] [c* exact-integer?])
         poly?]{
  Returns the poly resulting from taylor expanding the polynomial represented
  by @racket[p] at the point @racket[c*] in the variable @racket[v*].
  }

@section{Miscellaneous}
@defproc[(poly-show [p poly?]) any?]{
  Converts @racket[p] to a @racket[vector] and applies @racket[show] to the
  coefficients of its termlist.
  }
