#lang scribble/manual

@title[#:tag "implementation"]{Implementations}

The libraries @racket[(generic operations int)] and
@racket[(generic operations poly)] provide implementations of the arithmetic
types and generic operations introduced in the last section.
The polynomial library depends on another library
 @racket[(generic operations term)], which we discuss as well.

@include-section{int.scrbl}
@include-section{term.scrbl} 
@include-section{poly.scrbl}
