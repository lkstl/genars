;;; Construction
(define (alist . args*)
  (let loop ((args args*))
    (if (null? args) (list)
        (let ((head (car args))
              (tail (cdr args)))
          (if (null? tail)
              (error "alist: number of arguments must be even" args*)
              (alist-cons head (car tail) (loop (cdr tail))))))))
(define (table . args)
  (fold (lambda (pair tbl) (table-insert (car pair) (cdr pair) tbl))
        (alist)
        (apply alist args)))

;;; Predicates
(define (alist? x)
  (or (null? x) (and (pair? x) (pair? (car x)) (alist? (cdr x)))))
(define (table? x)
  (or (null? x)
      (and (pair? x)
           (let ((c (car x))) (and (pair? c) (list? (car c))))
           (table? (cdr x)))))

;;; Insertion
(define (table-insert keys value tbl)
  (let loop ((keys keys) (tbl tbl))
    (if (null? keys) value
        (let* ((key (car keys))
               (alis (if (alist? tbl) tbl (alist)))
               (entry (assoc key alis))
               (subtbl (if entry (cdr entry) (alist))))
          (alist-cons key (loop (cdr keys) subtbl) alis)))))

;;; Lookup
(define (table-lookup keys tbl)
  (let loop ((keys keys) (tbl tbl))
    (if (null? keys)
        tbl
        (if (alist? tbl)
            (let ((entry (assoc (car keys) tbl)))
              (if entry
                  (loop (cdr keys) (cdr entry))
                  #f))
            #f))))
