(define-library (util table)
  (import
    (scheme base)
    (only (srfi 1) alist-cons fold))
  (export
    alist table
    alist? table?
    table-lookup
    table-insert
    )
  (include "table.scm"))
