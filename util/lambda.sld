(define-library (util lambda)
  (import (scheme base)
          (only (srfi 1) reduce-right))
  (export lambda-constant lambda-identity lambda-compose)
  (begin
    (define (lambda-constant x) (lambda args x))
    (define lambda-identity (lambda args (apply values args)))
    (define (lambda-compose . fs)
      (define (lambda-compose f g)
        (lambda args
          (call-with-values
            (lambda () (apply g args))
            (lambda outputs (apply f outputs)))))
      (reduce-right lambda-compose lambda-identity fs))
    ))
