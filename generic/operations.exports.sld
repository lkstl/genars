(export
  int? poly? arithmetic-value? =?
  =zero? add neg mul div pow
  mod mod=zero? mod-add mod-neg mod-mul mod-div mod-pow
  make-ldiv
  make-euclid
  make-solve
  hensel
  taylor
  show
  )
