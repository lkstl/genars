(define type-tower (list 'Int 'Poly))
(define operation-table (table))

;;; Installing & looking up operations
(define (install-operation! proc name . types)
  (set! operation-table (table-insert (list name types) proc operation-table)))
(define (lookup-operation name . types)
  (table-lookup (list name types) operation-table))

;;; Type predicates & Equality
(define (int? x) ((lookup-operation 'in? 'Int) x))
(define (poly? x) ((lookup-operation 'in? 'Poly) x))
(define (arithmetic-value? x) (or (int? x) (poly? x)))
(define (detect-type x) 
  (let loop ((type-tower type-tower))
    (if (null? type-tower) #f
        (let* ((type (car type-tower))
               (in-type? (lookup-operation 'in? type)))
          (if (in-type? x) type (loop (cdr type-tower)))))))
(define (=? x y) (apply-generic '=? x y))

;;; Lifting, Projection & Simplification
(define (lift x)
  (let* ((type (detect-type x))
         (proc (lookup-operation 'lift type)))
    (if proc (proc x)
        (error "lift: no lifting procedure for this type" type))))
(define (project x)
  (let* ((type (detect-type x))
         (proc (lookup-operation 'project type)))
    (if proc (proc x)
        (error "project: no projection procedure for this type" type))))
(define (simplify x)
  (call/cc
    (lambda (k)
      (with-exception-handler
        (lambda (e) (k x))
        (lambda () (let ((x* (project x))) (if (=? x x*) (simplify x*) x)))))))

;;; Generic application
(define (apply-generic op . args)
  (define (lift-to arg type*)
    (let ((type (detect-type arg)))
      (if (equal? type type*) arg (lift-to (lift arg) type*))))
  (define (max-type types)
    ; returns the highest type in a list of types
    (if (null? type-tower)
        (error "max-type: type-tower is empty")
        (let loop ((types types)
                   (type-tower type-tower)
                   (current (car type-tower)))
          (cond ((null? types) current)
                ((null? type-tower)
                 (error "max-type: contains unknown type" types))
                (else (let ((next (car type-tower)))
                        (loop (delete next types) (cdr type-tower) next)))))))
  (define (next-type type)
    (let ((tail (member type type-tower)))
      (if tail (cadr tail) (error "next-type: unkown type or already maximal"
                                  type))))
  (define (compute-result)
    (let* ((types (map detect-type args))
           (proc (apply lookup-operation op types)))
      (cond (proc (apply proc args))
            ((null? types) (error "apply-generic: operation not available" op))
            (else
              (let ((types* (map (lambda-constant (max-type types)) types)))
                (let loop ((types* types*) (args* (map lift-to args types*)))
                  (let ((proc* (apply lookup-operation op types*)))
                    (if proc*
                        (apply proc* args*)
                        ; If op is not available for types*, set all of them
                        ; to the next type in the type-tower, lift
                        ; the arguments to this new type and repeat lookup.
                        ; If no higher type is available, next-type throws
                        ; an exception.
                        (with-exception-handler
                          (lambda (e) 
                            (error "apply-generic: no operation for these types"
                                   (list op types*)))
                          (lambda ()
                            (loop (map next-type types*)
                                  (map lift args*))))))))))))
  ; Compute the result of op when applied to args and subsequently simplify the
  ; results to the lowest type possible.
  (call-with-values compute-result
    (lambda results (apply values (map simplify results)))))

;;; Arithmetic
(define (=zero? x) (apply-generic '=zero? x))
(define (add . xs)
  (define (add x y) (apply-generic 'add x y))
  (reduce add 0 xs))
(define (neg x) (apply-generic 'neg x))
(define (mul . xs)
  (define (mul x y) (apply-generic 'mul x y))
  (reduce mul 1 xs))
(define (div x y) (apply-generic 'div x y))
(define (pow x n) (apply-generic 'pow x n))

;;; Modular arithmetic
(define (mod m) (lambda (x) (apply-generic 'mod x m)))
(define (mod=zero? m) (lambda (x) (=zero? ((mod m) x))))
(define (mod-add m) (lambda xs ((mod m) (apply add (map (mod m) xs)))))
(define (mod-neg m) (lambda (x) ((mod m) (neg ((mod m) x)))))
(define (mod-mul m) (lambda xs ((mod m) (apply mul (map (mod m) xs)))))
(define (mod-div m) (lambda (x y) (apply-generic 'mod-div m x y)))
(define (mod-pow m) (lambda (x n) ((mod m) (pow ((mod m) x) n))))

;;; Long division
(define (make-ldiv add neg mul div)
  (lambda (x y)
    (call-with-values
      (lambda () ((apply-generic 'ldiv x y) add neg mul div))
      (lambda (q r) (values (simplify q) (simplify r))))))
      ; we have to simplify manually because (apply-generic 'ldiv x y)
      ; returns a procedure not an arithmetic-value

;;; Extended euclidean algorithm
(define (make-euclid add neg mul div)
  (let ((ldiv (make-ldiv add neg mul div)))
    (lambda (x y)
      (let loop ((r0 x) (r1 y) (s0 1) (s1 0) (t0 0) (t1 1))
        (if (=zero? r1)
            (values r0 s0 t0)
            (let-values (((q r) (ldiv r0 r1)))
              (loop r1 r
                    s1 (add s0 (neg (mul q s1)))
                    t1 (add t0 (neg (mul q t1))))))))))

;;; Solving linear equations in two variables
(define (make-solve add neg mul div)
  (let ((ldiv (make-ldiv add neg mul div)))
    (lambda (g h s t a)
      (call-with-values
        (lambda () (ldiv (mul t a) g))
        (lambda (q t*) (values (add (mul s a) (mul h q)) t*))))))
                 
;;; Hensel lifting
(define (hensel m j f g h s t)
  (let loop ((g g) (h h) (s s) (t t) (m m) (i 1))
    (let ((solve (make-solve (mod-add m) (mod-neg m) (mod-mul m) (mod-div m))))
      (if (>= i j)
          (values g h s t)
          (let*-values
            (((v w) (solve g h s t (div (add f (neg (mul g h))) m)))
             ((g* h*) (values (add g (mul m w)) (add h (mul m v))))
             ((v* w*) (solve g h s t (div (add (mul g* s) (mul h* t) -1) m)))
             ((s* t*) (values (add s (neg (mul m v*)))
                              (add t (neg (mul m w*))))))
            (loop g* h* s* t* (pow m 2) (* i 2)))))))

;;; Taylor expansion
(define (taylor v c) (lambda (p) (simplify ((apply-generic 'taylor p) v c))))

;;; Miscellaneous
(define (show x) (apply-generic 'show x))
