(define-library (generic operations)
  (import (scheme base)
          (only (srfi 1) delete reduce)
          (util table)
          (util lambda))
  (export
    type-tower operation-table
    install-operation! lookup-operation
    int? poly? arithmetic-value? detect-type =?
    lift project simplify
    apply-generic)
  (include-library-declarations "operations.exports.sld")
  (include "operations.scm"))
