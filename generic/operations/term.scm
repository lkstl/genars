;;; Construction
(define (term order coeff) (cons order coeff))
(define (termlist-cons t ts) (if (=zero? (get-coeff t)) ts (cons t ts)))
(define termlist-null '())
(define (termlist . terms) (fold-right termlist-cons termlist-null terms))

;;; Selection
(define (get-order t) (car t))
(define (get-coeff t) (cdr t))

;;; Predicates
(define (term? x)
  (and (pair? x)
       (let ((c (car x))) (and (exact-integer? c) (not (negative? c))))
       (arithmetic-value? (cdr x))))
(define (termlist? x)
  (and (list? x)
       (let loop ((x x) (o +inf.0))
         (if (null? x) #t
             (let ((t (car x)))
               (and (term? t)
                    (let ((o* (get-order t)))
                      (and (< o* o)
                           (loop (cdr x) o*)))))))))

;;; Mapping
(define (termlist-map order-op coeff-op ts)
  ; order-op : term -> order (nonnegative integer)
  ; coeff-op : term -> coeff (arithmetic value)
  (let loop ((result (termlist)) (ts ts))
    (if (null? ts)
        (reverse result)
        (let ((t (car ts)))
          (loop (termlist-cons (term (order-op t) (coeff-op t)) result)
                (cdr ts))))))

;;; Arithmetic
(define (termlist-add add) 
  (lambda (ts1 ts2)
    (let loop ((ts1 ts1) (ts2 ts2))
      (cond
        ((null? ts1) ts2)
        ((null? ts2) ts1)
        (else (let* ((t1 (car ts1)) (o1 (get-order t1)) 
                                    (t2 (car ts2)) (o2 (get-order t2)))
                (cond ((> o1 o2) (cons t1 (loop (cdr ts1) ts2)))
                      ((< o1 o2) (cons t2 (loop ts1 (cdr ts2))))
                      (else (termlist-cons
                              (term o1 (add (get-coeff t1) (get-coeff t2)))
                              (loop (cdr ts1) (cdr ts2)))))))))))

(define (termlist-neg neg)
  (lambda (ts)
    (termlist-map get-order (lambda (t) (neg (get-coeff t))) ts)))

(define (termlist-mul add mul)
  (lambda (ts1 ts2)
    (let loop ((ts1 ts1))
      (if (null? ts1)
          (termlist)
          (let* ((t1 (car ts1)) (o1 (get-order t1)) (c1 (get-coeff t1)))
            ((termlist-add add)
             (termlist-map (lambda (t2) (+ o1 (get-order t2)))
                           (lambda (t2) (mul c1 (get-coeff t2)))
                           ts2)
             (loop (cdr ts1))))))))

(define (termlist-div add neg mul div) 
  (lambda (ts1 ts2)
    (call-with-values
      (lambda () ((termlist-ldiv add neg mul div) ts1 ts2))
      (lambda (tsq tsr)
        (if (null? tsr) tsq
            (error "termlist-div: division impossible" ts1 ts2))))))

;;; Modular arithmetic
(define (termlist-mod ts tsm)
  ; tsm must contain at most one term, which is constant or monic
  (define (termlist-mod-term tm)
    (let ((om (get-order tm)) (cm (get-coeff tm)))
      (cond
        ((zero? om)
         (termlist-map get-order (lambda (t) ((mod cm) (get-coeff t))) ts))
        ((=? 1 cm) (drop-while (lambda (t) (>= (get-order t) om)) ts))
        (else
          (error "termlist-mod-term: expected constant or monic term" tm)))))
  (cond ((null? tsm) ts)
        ((null? (cdr tsm))
         (termlist-mod-term (car tsm)))
        (else (error "termlist-mod: modulus must contain at most one term"
                     tsm))))

(define (termlist-mod-div tsm ts1 ts2)
  (define (termlist-mod-term-div tm)
    (let ((om (get-order tm)) (cm (get-coeff tm)))
      (cond
        ((zero? om) ((termlist-div (mod-add cm) (mod-neg cm) (mod-mul cm)
                                   (mod-div cm)) ts1 ts2))
        ((=? 1 cm) ((termlist-div< om add neg mul div) ts1 ts2))
        (else (error "termlist-mod-term-dv: expected constant or monic term"
                     tm)))))
  (let ((ts1 (termlist-mod ts1 tsm)) (ts2 (termlist-mod ts2 tsm)))
    (cond ((null? tsm) ((termlist-div add neg mul div) ts1 ts2))
          ((null? (cdr tsm)) (termlist-mod-term-div (car tsm)))
          (else (error "termlist-mod-div: modulus must contain at most
                       one term" tsm)))))

(define (termlist-div< n add neg mul div)
  (lambda (ts1 ts2)
    (let ((ts2 (reverse ts2)))
      (let* ((t2 (car ts2))
             (ord2 (get-order t2))
             (c2 (get-coeff t2))
             (tail2 (cdr ts2)))
        (let loop ((tsq (termlist)) (tsr (reverse ts1)))
          (if (null? tsr) tsq
              (let* ((tr (car tsr))
                     (ordr (get-order tr))
                     (cr (get-coeff tr))
                     (ordq (- ordr ord2)))
                (cond ((< ordq 0) (error "termlist-div<: division
                                         not possible" (list tsr ts2)))
                      ((>= ordq n) tsq)
                      (else (let* ((cq (div cr c2)) (tq (term ordq cq)))
                              (loop (termlist-cons tq tsq)
                                    ((termlist-add add)
                                     (cdr tsr)
                                     ((termlist-mul add mul)
                                      ((termlist-neg neg) (termlist tq))
                                      tail2)))))))))))))

;;; Long division
(define (termlist-ldiv add neg mul div)
  (lambda (ts1 ts2)
    (if (null? ts2)
        (error "%termlist-ldiv: division by zero" (list ts1 ts2))
        (let* ((t2 (car ts2))
               (ord2 (get-order t2))
               (c2 (get-coeff t2))
               (tail2 (cdr ts2)))
          (let loop ((tsq (termlist)) (tsr ts1))
            (if (null? tsr)
                (values (reverse tsq) tsr)
                (let* ((tr (car tsr))
                       (ordr (get-order tr))
                       (cr (get-coeff tr)))
                  (if (< ordr ord2)
                      (values (reverse tsq) tsr)
                      (let* ((ordq (- ordr ord2))
                             (cq (div cr c2))
                             (tq (term ordq cq)))
                        (loop (termlist-cons tq tsq)
                              ((termlist-add add)
                               (cdr tsr)
                               ((termlist-mul add mul)
                                ((termlist-neg neg) (termlist tq))
                                tail2))))))))))))

;;; Taylor expansion
(define (termlist-ev c ts)
  (fold (lambda (t e) (add (mul (get-coeff t) (pow c (get-order t))) e)) 0 ts))

(define (termlist-hasse k ts)
  (define (binom n k)
    (cond ((or (< n k) (< k 0)) 0)
          ((or (= n 0) (= n k)) 1)
          (else (+ (binom (- n 1) (- k 1)) (binom (- n 1) k)))))
  (let ((ts (take-while (lambda (t) (>= (get-order t) k)) ts)))
    (termlist-map
      (lambda (t) (- (get-order t) k))
      (lambda (t) (mul (binom (get-order t) k) (get-coeff t)))
      ts)))

(define (termlist-taylor c ts)
  (define (taylor-coeff k) (termlist-ev c (termlist-hasse k ts)))
  (if (null? ts) ts
      (let ((n (get-order (car ts))))
        (let loop ((k 0) (ts* (termlist)))
          (if (> k n) ts*
              (loop (+ k 1) (termlist-cons (term k (taylor-coeff k)) ts*)))))))
