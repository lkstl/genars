(define-library (generic operations term)
  (import (scheme base)
          (only (srfi 1) fold fold-right drop-while take-while)
          (generic operations))
  (export term termlist-cons termlist-null termlist
          get-order get-coeff
          term? termlist?
          termlist-map
          termlist-add termlist-neg termlist-mul termlist-div termlist-ldiv
          termlist-mod termlist-mod-div
          termlist-taylor
          )
  (include "term.scm"))
