;;; Record type declaration
(define-record-type Poly
  (poly var center terms)
  poly?
  (var get-var)
  (center get-center)
  (terms get-terms))

;;; Equality
(define (poly=? p1 p2) (poly=zero? (poly-add p1 (poly-neg p2))))

;;; Lifting & Projecting
(define (poly-embed v*)
  (lambda (p)
    (if (not (poly? p))
        (poly v* 0 (termlist (term 0 p)))
        (let ((v (get-var p)))
          (cond ((var>? v* v) (poly v* 0 (termlist (term 0 p))))
                ((var<? v* v)
                 (error "poly-embed: given variable is smaller
                        than that of given polynomial" (list v* v)))
                (else p))))))

(define (const-coeff p)
  (let ((ts (get-terms p)))
    (if (null? ts) 0
        (let ((t (last ts)))
          (if (zero? (get-order t)) (get-coeff t) 0)))))

;;; Variable ordering
(define (var<? v1 v2)
  (and v2 (or (not v1) (string<? (symbol->string v1) (symbol->string v2)))))
(define (var>? v1 v2) (var<? v2 v1))

;;; Applying termlist-operations
(define (poly-apply termlist-op . ps)
  (define (first-highest-var-poly ps)
    (if (null? ps)
        (error "first-highest-var-poly: expected nonempty list of polys" ps)
        (let loop ((ps (cdr ps)) (p* (car ps)))
          (if (null? ps) p*
              (let ((p (car ps)))
                (loop (cdr ps) (if (var>? (get-var p) (get-var p*)) p p*)))))))
  (define (%poly-taylor v* c*) (lambda (p) ((poly-taylor p) v* c*)))
  (let* ((p* (first-highest-var-poly ps))
         (v* (get-var p*))
         (c* (get-center p*))
         (ps* (map (lambda-compose (%poly-taylor v* c*) (poly-embed v*)) ps)))
    (let-values ((result-termlists (apply termlist-op (map get-terms ps*))))
      (apply values (map (lambda (ts) (poly v* c* ts))
                         result-termlists)))))

;;; Arithmetic
(define (poly=zero? p) (null? (get-terms p)))
(define (poly-add p1 p2) (poly-apply (termlist-add add) p1 p2))
(define (poly-neg p) (poly-apply (termlist-neg neg) p))
(define (poly-mul p1 p2) (poly-apply (termlist-mul add mul) p1 p2))
(define (poly-div p1 p2)
  (poly-apply (termlist-div add neg mul div) p1 p2))
(define (poly-pow p n)
  (if (negative? n)
      (error "poly-pow: expected nonnegative exponent" n)
      (let loop ((p p) (n n) (result ((poly-embed (get-var p)) 1)))
        (cond ((zero? n) result)
              ((even? n) (loop (poly-mul p p) (/ n 2) result))
              (else (loop p (- n 1) (poly-mul p result)))))))

;;; Modular arithmetic
(define (poly-mod p1 p2)
  (let ((v1 (get-var p1)) (c1 (get-center p1))
        (v2 (get-var p2)) (c2 (get-center p2)))
    (cond
      ((var<? v1 v2) p1)
      ((var>? v1 v2)
       (poly v1 ((mod p2) c1)
         (termlist-map get-order (lambda (t) ((mod p2) (get-coeff t)))
                       (get-terms p1))))
      (else
        (let ((p1* ((poly-taylor p1) v2 c2)))
          (poly v2 c2 (termlist-mod (get-terms p1*) (get-terms p2))))))))
(define (poly-mod-div m p1 p2)
  (poly-apply termlist-mod-div m (poly-mod p1 m) (poly-mod p2 m)))

;;; Long division
(define (poly-ldiv p1 p2)
  (lambda (add neg mul div)
    (poly-apply (termlist-ldiv add neg mul div) p1 p2)))

;;; Taylor expansion
(define (poly-taylor p)
  (lambda (v* c*)
    (if (exact-integer? c*)
        (let ((v (get-var p)) (c (get-center p)) (ts (get-terms p)))
          (cond
            ((var>? v* v) p)
            ((var<? v* v)
             (poly v c
               (termlist-map
                 get-order (lambda (t) ((taylor v* c*) (get-coeff t))) ts)))
            ((=? c* c) p)
            (else (poly v c* (termlist-taylor (add c* (neg c)) ts)))))
        (error "poly-taylor: expected exact integer" c*))))

;;; Miscellaneous
(define (poly-show p)
  (let ((v (get-var p)))
    (if (not v) (show (const-coeff p))
        (vector v (get-center p)
          (map (lambda (t) (cons (get-order t) (show (get-coeff t))))
               (get-terms p))))))


;;; Installation
(install-operation! poly? 'in? 'Poly)
(install-operation! poly=? '=? 'Poly 'Poly)
(install-operation! const-coeff 'project 'Poly)
(install-operation! (poly-embed #f) 'lift 'Int)
(install-operation! poly-show 'show 'Poly)
(install-operation! poly=zero? '=zero? 'Poly)
(install-operation! poly-add 'add 'Poly 'Poly)
(install-operation! poly-neg 'neg 'Poly)
(install-operation! poly-mul 'mul 'Poly 'Poly)
(install-operation! poly-div 'div 'Poly 'Poly)
(install-operation! poly-pow 'pow 'Poly 'Int)
(install-operation! poly-mod 'mod 'Poly 'Poly)
(install-operation! poly-mod-div 'mod-div 'Poly 'Poly 'Poly)
(install-operation! poly-ldiv 'ldiv 'Poly 'Poly)
(install-operation! poly-taylor 'taylor 'Poly)
