Genars
=====
A generic arithmetic system for multivariate polynomials over the integers.
Implemented in Scheme (r7rs compliant).

Use the [Chibi-scheme](https://github.com/ashinn/chibi-scheme/) interpreter

```
> chibi-scheme -A./
```

 and import the main library with

```
> (import (generic system))
```

The [documentation](https://lkstl.gitlab.io/genars/) uses [Scribble](https://docs.racket-lang.org/scribble/index.html).
